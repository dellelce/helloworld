
#
# /home/antonio/src/bitbucket/helloworld, 2013 by Antonio Dell'elce, antonio@dellelce.com
#

CC             = gcc
TARGET         = helloworld

SHELL          = /bin/bash

CFILES         = helloworld.c
OFILES         = helloworld.o
LDFLAGS	       = 


LOC_HFILES     = 
HFILES         = $(LOC_HFILES)

INCLUDES       =  -I.
DEBUG          = 
CFLAGS         = -Wall -O2  $(INCLUDES) $(DEBUG)
LIBS           = 


#
# --- RULES ---
#

all: $(TARGET)

$(TARGET):   $(OFILES)
	@echo "LINK " $(TARGET)
	@$(CC) -o $(TARGET) $(LDFLAGS) $(OFILES) $(LIBS)

#
# -- DEPS --
#

helloworld.o: helloworld.c $(HFILES) 
	@echo "CC "helloworld.c
	@$(CC) -c $(CFLAGS) -o helloworld.o helloworld.c

 
#  
# clean
#    
     
clean:
	rm -f $(TARGET) $(OFILES) $(LOC_HFILES) *.exe

#
# redo
#

redo: clean all

