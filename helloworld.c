/* 

   File:    helloworld.c
   Created: 1539 280613

*/

// includes

#include <stdio.h>

// main

int
main (int argc, char **argv)
{
  printf ("hello bitbucket!\n");

  // generic return

  return 0;
}


// ** EOF **
